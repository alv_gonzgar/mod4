$(function(){
	$('.carousel').carousel({
		interval: 3000
	});

	$('#modalLoginForm').on('show.bs.modal', function(e){
		console.log('Evento show');
		$('#modalbtn').removeClass('btn btn-primary');
		$('#modalbtn').addClass('btn btn-default');
	});

	$('#modalLoginForm').on('shown.bs.modal', function(e){
		console.log("Evento shown");
	});

	$('#modalLoginForm').on('hide.bs.modal', function(e){
		console.log("Evento hide");
		$('#modalbtn').removeClass('btn btn-default');
		$('#modalbtn').addClass('btn btn-primary');
	});

	$('#modalLoginForm').on('hidden.bs.modal', function(e){
		console.log("Evento hidden");
	});

});